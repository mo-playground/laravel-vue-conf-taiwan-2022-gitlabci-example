<?php

namespace Tests\Unit\Services;

use App\Services\Example;
use PHPUnit\Framework\TestCase;

class ExampleTest extends TestCase
{
    public function testIsTrue()
    {
        $target = new Example();
        $this->assertTrue($target->isTrue());
    }

    public function testIsNot()
    {
        $target = new Example();
        $this->assertTrue($target->not(false));
    }
}
