<?php

namespace App\Services;

class Example
{
    public function isTrue(): bool
    {
        return true;
    }

    public function isFalse(): bool
    {
        return false;
    }

    public function not(bool $bool): bool
    {
        return ! $bool;
    }
}
